import React, { Dispatch, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { ConfigProvider,notification } from 'antd';

import {
  LogoutPage,
  ShopPage,
  VoicePage,
  EventsPage,
  NewsPage,
  BenefitsPage,
  DebugPage,
  ProfilePage,
} from '../pages';
import { resetUser,  setUser } from '../features/slices/authSlice';
import { RootState, store } from '../features/store';
import { authApi } from '../features/api/authApi';
import { iterateCoin, selectIsInit, setIsInit } from '../features/slices/appSlice';
import { MainLayout } from '../entities';
import './global.css';


const initApp = async (dispatch: Dispatch<any>) => {
  const token = localStorage.getItem('accessToken');

  if (token) {
    const { data, isSuccess, isError } = (await dispatch(
      authApi.endpoints.getMe.initiate('')
    )) as any;

    if (isSuccess) {
      dispatch(setUser(data));
    } else if (isError) {
      dispatch(resetUser());
    }
  }

  await dispatch(setIsInit());
};

store.dispatch(initApp);


function App() {
  const dispatch = useDispatch()
  const isInit = useSelector((state: RootState) => selectIsInit(state));

  useEffect(()=>{
    const openNotification = () => {
      notification.open({
        type:'success',
        message: 'Вам начислен 1 bcc-coin',
      });
      dispatch(iterateCoin())
    };
    
    if(!localStorage.getItem('isTaked')){
      localStorage.setItem('isTaked', 'true')
      openNotification()
    }
  },[notification])

  if (!isInit) {
    return <>...loading</>;
  }

  return (
    <ConfigProvider
      theme={
        {
          // token: {
          //   colorPrimary: '#0d6efd'
          // }
        }
      }
    >
      <BrowserRouter>
        <Routes>
            <Route path="/" element={<MainLayout />}>
              <Route index element={<NewsPage />} />
              <Route path="shop" element={<ShopPage />} />
              <Route path="voice" element={<VoicePage />} />
              <Route path="events" element={<EventsPage />} />
              <Route path="benefits" element={<BenefitsPage />} />
              <Route path="debug" element={<DebugPage />} />
              <Route path="profile" element={<ProfilePage />} />
              <Route path="logout" element={<LogoutPage />} />
              {/* <Route path="*" element={<Navigate to="/" />} /> */}
            </Route>
        </Routes>
      </BrowserRouter>
    </ConfigProvider>
  );
}

export default App;

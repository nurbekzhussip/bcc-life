import React, { FC } from "react";
import { NavLink } from "react-router-dom";
import { Avatar, Col, Popover, Row } from "antd";
import {
  UserOutlined,
  MoneyCollectTwoTone,
  AppstoreOutlined,
} from "@ant-design/icons";
import s from "./style.module.css";
import { numberWithSpaces } from "../../shared/helpers/numberFormat";
import { Input, Space } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "../../features/store";
import { selectCoins } from "../../features/slices/appSlice";
import imgConfluence from "../../assets/confluence.png";
import imgPortal from "../../assets/portal.svg";
import imgServiceDesk from "../../assets/service-desc.svg";
import imgJira from "../../assets/jira.png";
import imgGitlab from "../../assets/gitlab.png";
import imgElma from "../../assets/elma.svg";
import imgCoin from '../../assets/coin.png'

const { Search } = Input;

const links = [
  {
    title: "Confluence",
    image: imgConfluence,
    href: "https://kb.apps.bcc.kz",
  },
  {
    title: "Portal",
    image: imgPortal,
    href: "https://kb.apps.bcc.kz",
  },
  {
    title: "Internal",
    image: imgPortal,
    href: "http://int.cbank.kz/",
  },
  {
    title: "Service-desc",
    image: imgServiceDesk,
    href: "https://service-desk.bank.corp.centercredit.kz/portal/",
  },
  {
    title: "Jira",
    image: imgJira,
    href: "https://jira.bcc.kz/",
  },
  {
    title: "Jira",
    image: imgGitlab,
    href: "https://gitlab.apps.bcc.kz/",
  },
  {
    title: "Elma",
    image: imgElma,
    href: "https://elma.bcc.kz/",
  },
];

const content = (
  <div className={s.links}>
    <Row gutter={16}>
      {links.map((link) => {
        return (
          <Col key={link.href} span={6}>
            <div key={link.href} className={s.item}>
              <a href={link.href} className={s.linkItem} target="_blank">
                <img src={link.image} alt="" />
                <span>{link.title}</span>
              </a>
            </div>
          </Col>
        );
      })}
    </Row>
  </div>
);

const Header: FC = () => {
  const myCoin = useSelector((state: RootState) => selectCoins(state));
  const onSearch = (value: string) => console.log(value);

  return (
    <div className={s.header}>
      <ul>
        <li className={s.wallet}>
          {numberWithSpaces(myCoin)} <img src={imgCoin} style={{
                width:'16px'
            }}/>
        </li>
        <li className={s.search}>
          <Search
            placeholder="Введите текст"
            allowClear
            enterButton="Найти"
            onSearch={onSearch}
          />
        </li>
        
        <li>
          <Popover
            placement="bottomRight"
            content={content}
            title="Все полезные ссылки"
            trigger="click"
          >
             <Avatar shape="square" icon={<AppstoreOutlined />} style={{backgroundColor:'#24AE60'}}/>
          </Popover>
        </li>
        <li>
          <NavLink to="/profile"><Avatar icon={<UserOutlined />} /></NavLink>
        </li>
        {/* <li>
          <NavLink to="/logout">Выйти</NavLink>
        </li> */}
      </ul>
    </div>
  );
};

export default Header;

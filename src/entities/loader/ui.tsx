import React, { FC } from 'react';
import { Spin } from 'antd';
import s from './style.module.css';

const Loader: FC = () => {
  return (
    <div className={s.wrap}>
      <Spin tip="Загрузка...">
        <div />
      </Spin>
    </div>
  );
};

export default Loader;

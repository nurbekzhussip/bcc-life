import React, { FC} from 'react';
import { Button, Form, Input, Select } from 'antd';
import {
  useGetTemplatesQuery,
} from '../../features/api/templateApi';
import { ITemplate } from '../../shared/interfaces/Template';
import { usePostMailingMutation } from '../../features/api/messengerApi';
import s from './style.module.css';

interface IProps {
  fromIds: number[];
  onClose: () => void;
}

const { TextArea } = Input;

const MailingForm: FC<IProps> = ({ fromIds, onClose }) => {
  const [form] = Form.useForm();
  const { data: templatesData, isFetching: isTemplatesFetching } =
    useGetTemplatesQuery('');
  const [postMailing, { isLoading: isMailingLoading }] =
    usePostMailingMutation();

  const onSelect = (templateId: number) => {
    const findTemplate = templatesData?.items?.find(
      (template: ITemplate) => template.id === templateId
    );

    if (findTemplate?.text) {
      form.setFieldValue('text', findTemplate.text);
    }
  };

  const onFinish = (values: any) => {
    postMailing({
      fromIds: fromIds,
      message: values.text
    }).then(onClose);
  };

  return (
    <>
      <Form
        name="sendMailing"
        form={form}
        onFinish={onFinish}
        layout="vertical"
      >
        <Form.Item
          label="Готовые шаблоны"
        >
          <Select
            showSearch
            placeholder="Поиск по шаблону"
            optionFilterProp="label"
            className={s.search}
            loading={isTemplatesFetching}
            options={templatesData?.items?.map((template: ITemplate) => ({
              label: template.title,
              value: template.id
            }))}
            onSelect={onSelect}
          />
        </Form.Item>

        <Form.Item
          name="text"
          label="Текст"
          rules={[{ required: true, message: '' }]}
        >
          <TextArea
            showCount
            maxLength={4096}
            rows={4}
            autoSize={{ minRows: 4, maxRows: 4 }}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isMailingLoading}>
            Отправить
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default MailingForm;

import React, { FC, useState } from 'react';
import { Outlet } from 'react-router-dom';
import { Layout } from 'antd';
import { Sidebar, Header as CustomHeader } from '../index';
import s from './style.module.css';

const { Header, Content, Sider } = Layout;

const MainLayout: FC = () => {
  const [collapsed, setCollapsed] = useState(true);

  return (
    <Layout className={s.layout}>
      <Sider
        collapsible
        theme='light'
        collapsed={collapsed}
        onCollapse={value => setCollapsed(value)}
        className={s.uiSider}
      >
        <Sidebar/>
      </Sider>
      <Layout className="site-layout">
        <Header className={s.uiHeader}>
          <CustomHeader />
        </Header>
        <Content className={s.uiContent}>
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;

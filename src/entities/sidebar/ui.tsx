import React, { FC } from 'react';
import { Menu, MenuProps } from 'antd';
import {
  UserOutlined,
  HomeOutlined,
  DiffOutlined,
  AppstoreAddOutlined,
  BankOutlined,
  CalendarOutlined,
  BorderOutlined,
  BarsOutlined,
  AudioOutlined,
  RobotOutlined
} from '@ant-design/icons';
import { NavLink, useLocation } from 'react-router-dom';
import BccLogo from '../../assets/logo.png';
import s from './style.module.css';

const items = [
  {
    key: '/',
    label: <NavLink to="/">Главная</NavLink>,
    icon: <HomeOutlined />,
    children: null
  },
  {
    key: '/voice',
    label: <NavLink to="/voice">BCC Voice</NavLink>,
    icon: <AudioOutlined />,
    children: null
  },
  // {
  //   key: '/events',
  //   label: <NavLink to="/events">Events</NavLink>,
  //   icon: <CalendarOutlined />,
  //   children: null
  // },
  {
    key: '/benefits',
    label: <NavLink to="/benefits">Бенефиты</NavLink>,
    icon: <BarsOutlined />,
    children: null
  },
  {
    key: '/shop',
    label: <NavLink to="/shop">BCC Shop</NavLink>,
    icon: <AppstoreAddOutlined />,
    children: null
  },
  {
    key: '/debug',
    label: <NavLink to="/debug">Тестирование</NavLink>,
    icon: <RobotOutlined />,
    children: null
  }
];

const Sidebar: FC = () => {
  const { pathname } = useLocation();

  const onClick: MenuProps['onClick'] = e => {
  };

  return (
    <>
      <div className={s.logo}>
        <NavLink to="/">
        <img  src={BccLogo} alt="Bcc Logo"/>
          </NavLink>
      </div>
      <Menu
        onClick={onClick}
        theme="light"
        defaultSelectedKeys={[pathname]}
        mode="inline"
        items={items}
      />
    </>
  );
};

export default Sidebar;

import React, { FC, useEffect } from 'react';
import { Button, Form, Input } from 'antd';
import { ITemplate } from '../../shared/interfaces/Template';
import {
  useCreateTemplateMutation,
  useUpdateTemplateMutation
} from '../../features/api/templateApi';

interface IProps {
  item: ITemplate;
  onClose: () => void;
}

const { TextArea } = Input;

const TemplateForm: FC<IProps> = ({ item, onClose }) => {
  const [form] = Form.useForm();
  const [updateTemplate, { isLoading: isUpdateLoading }] =
    useUpdateTemplateMutation();
  const [createTemplate, { isLoading: isCreateLoading }] =
    useCreateTemplateMutation();

  useEffect(() => {
    if (item) {
      form.setFieldsValue({
        title: item.title,
        text: item.text
      });
    }
  }, [item]);

  const onFinish = (values: any) => {
    let query = null;
    if (item) {
      query = updateTemplate({
        title: values.title,
        text: values.text
      });
    } else {
      query = createTemplate({
        title: values.title,
        text: values.text
      });
    }

    query?.then(() => {
      onClose();
    });
  };

  return (
    <Form name="sendMessage" form={form} onFinish={onFinish} layout="vertical">
      <Form.Item
        name="title"
        label="Заголовок"
        rules={[{ required: true, message: 'Please type a message!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="text"
        label="Текст"
        rules={[{ required: true, message: 'Please type a text!' }]}
      >
        <TextArea
          showCount
          maxLength={4096}
          rows={4}
          autoSize={{ minRows: 4, maxRows: 4 }}
        />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          loading={isCreateLoading || isUpdateLoading}
        >
          Отправить
        </Button>
      </Form.Item>
    </Form>
  );
};

export default TemplateForm;

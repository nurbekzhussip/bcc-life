import React, { FC, useEffect } from 'react';
import { Button,  Form, Input, Select } from 'antd';
import { useGetUserQuery, useUpdateUserMutation } from '../../../features/api/userApi';
import { UserRolesEnum } from '../../../shared/enums/User';

interface IProps {
  userId: number;
  onClose: () => void
}

const UserEdit: FC<IProps> = ({ userId,onClose }) => {
  const [form] = Form.useForm();
  const { data, isSuccess } = useGetUserQuery(userId, {
    skip: !userId
  });

  const [updateUser, {isLoading}] = useUpdateUserMutation()

  useEffect(()=>{
    if(isSuccess){
      form.setFieldsValue({
        email: data.email,
        role: data.role
      })
    }
  },[isSuccess, data])

  const onFinish = (values:any) => {
    updateUser({
      userId:data.id,
      role: values.role
    }).then(()=>onClose())
  };

  return (
    <>
      <Form
      key={data?.id}
        name="edit_user"
        form={form}
        onFinish={onFinish}
      >
        <Form.Item name="email" label="Email">
          <Input placeholder="Email" disabled/>
        </Form.Item>
        <Form.Item name="role" label="Роль">
          <Select
            options={Object.values(UserRolesEnum).map(role => ({
              value: role,
              label: role
            }))}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default UserEdit;

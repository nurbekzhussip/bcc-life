import { baseEmptyAPI } from './baseQuery';

export const authApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    login: builder.mutation({
      query: body => ({
        url: `/auth/login`,
        method: 'POST',
        body
      }),
      invalidatesTags: ['USERS']
    }),
    signup: builder.mutation({
      query: body => ({
        url: `/auth/signup`,
        method: 'POST',
        body
      }),
      invalidatesTags: ['USERS']
    }),
    logout: builder.query({
      query: () => `/auth/logout`
    }),
    getMe: builder.query({
      query: () => `/auth/getMe`
    })
  })
});

export const {
  useLoginMutation,
  useGetMeQuery,
  useLogoutQuery,
  useSignupMutation
} = authApi;

import {
  BaseQueryFn,
  createApi,
  FetchArgs,
  fetchBaseQuery
} from '@reduxjs/toolkit/query/react';
import { resetUser } from '../slices/authSlice';

interface CustomError {
  data: {
    code: number;
    message: string;
  };
  status: number;
}

const dynamicBaseQuery = fetchBaseQuery({
  baseUrl: '/api',
  prepareHeaders: (headers, { getState }) => {
    const accessToken = localStorage.getItem('accessToken');

    if (accessToken) {
      headers.set('Authorization', 'Bearer ' + accessToken);
    }
    
    return headers;
  },
  credentials: 'include'
}) as BaseQueryFn<string | FetchArgs, unknown, CustomError>;

const baseQuery: BaseQueryFn<string | FetchArgs, unknown, CustomError> = async (
  args,
  api,
  extraOptions
) => {
  const req = await dynamicBaseQuery(args, api, extraOptions);
  
  if(req.error?.status === 401){
    api.dispatch(resetUser())
  }
  return req
};

export const baseEmptyAPI = createApi({
  reducerPath: 'api',
  baseQuery: baseQuery,
  tagTypes: ['USERS','SCENARIOS', 'QUESTIONS', 'TEMPLATES'],
  endpoints: () => ({})
});

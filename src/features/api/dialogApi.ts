import { baseEmptyAPI } from './baseQuery';

export const dialogApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    getDialogs: builder.query({
      query: () => `/dialogs`
    }),
    getHistoryByFromId:builder.query({
      query: (fromId) => `/dialogs/${fromId}`
    })
  })
});

export const { useGetDialogsQuery, useGetHistoryByFromIdQuery } = dialogApi;

import { baseEmptyAPI } from './baseQuery';

export const memberApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    getMembersWithLastMessage: builder.query({
      query: () => `/members/message`
    }),
    getMembers: builder.query({
        query: () => `/members?page=1&limit=100`
      })
  })
});

export const { useGetMembersWithLastMessageQuery, useGetMembersQuery } = memberApi;

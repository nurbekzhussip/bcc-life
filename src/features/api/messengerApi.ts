import { baseEmptyAPI } from './baseQuery';

export const messengerApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    postMailing: builder.mutation({
      query: body => ({
        url: `/messenger/mailing`,
        method: 'POST',
        body
      }),
    })
  })
});

export const { usePostMailingMutation
} = messengerApi;

import { baseEmptyAPI } from './baseQuery';

export const questionApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    getQuestions: builder.query({
      query: () => `/questions`,
      providesTags: ['QUESTIONS']
    }),
    createQuestion: builder.mutation({
      query: body => ({
        url: '/questions',
        method: 'POST',
        body
      }),
      invalidatesTags: (result, error, arg) => (error ? [] : ['QUESTIONS'])
    }),
    updateQuestion: builder.mutation({
      query: ({ questionId, ...body }) => ({
        url: `/questions/${questionId}`,
        method: 'PUT',
        body
      }),
      invalidatesTags: (result, error, arg) => {
        if (error) {
          return [];
        }
        return [{ type: 'QUESTIONS', id: arg.questionId }];
      }
    }),
    deleteQuestion: builder.mutation({
      query: ({questionId}) => ({
        url: `/questions/${questionId}`,
        method: 'DELETE'
      }),
      invalidatesTags: (result, error, arg) => {
        if (error) {
          return [];
        }
        return [{ type: 'QUESTIONS', id: arg.questionId }];
      }
    }),
    createAnswerByQId: builder.mutation({
      query: ({ questionId, ...body }) => ({
        url: `/questions/${questionId}/answers`,
        method: 'POST',
        body
      }),
      invalidatesTags: (result, error, arg) => {
        if (error) {
          return [];
        }
        return [
          { type: 'QUESTIONS', id: arg.questionId },
        ];
      }
    }),
    updateAnswerByQId: builder.mutation({
      query: ({ questionId, answerId, ...body }) => ({
        url: `/questions/${questionId}/answers/${answerId}`,
        method: 'PUT',
        body
      })
    })
  })
});

export const {
  useGetQuestionsQuery,
  useCreateQuestionMutation,
  useUpdateQuestionMutation,
  useDeleteQuestionMutation,
  useCreateAnswerByQIdMutation,
  useUpdateAnswerByQIdMutation
} = questionApi;

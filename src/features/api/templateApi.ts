import { baseEmptyAPI } from './baseQuery';

export const templateApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    getTemplates: builder.query({
      query: () => `/templates?page=1&limit=100`,
      providesTags: ['TEMPLATES']
    }),
    getTemplateById: builder.query({
      query: (templateId) => `/templates/${templateId}`,
    }),
    createTemplate: builder.mutation({
      query: body => ({
        url: `/templates`,
        method: 'POST',
        body
      }),
      invalidatesTags: ['TEMPLATES']
    }),
    deleteTemplate:builder.mutation({
      query: (templateId) => ({
        url: `/templates/${templateId}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['TEMPLATES']
    }),
    updateTemplate:builder.mutation({
      query: ({templateId, ...body}) => ({
        url: `/templates/${templateId}`,
        method: 'PUT',
        body
      }),
      invalidatesTags: ['TEMPLATES']
    })
  })
});

export const { useGetTemplatesQuery, useGetTemplateByIdQuery, useCreateTemplateMutation, useDeleteTemplateMutation, useUpdateTemplateMutation } = templateApi;

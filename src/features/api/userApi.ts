import { baseEmptyAPI } from './baseQuery';

export const userApi = baseEmptyAPI.injectEndpoints({
  endpoints: builder => ({
    getUsers: builder.query({
      query: ({ page, limit }) => `/users?limit=${limit}&page=${page}`,
      providesTags: ['USERS']
    }),
    getUser:builder.query({
      query: (userId) => `/users/${userId}`,
      providesTags: (result, error, arg) => [{type:'USERS', id:arg}]
    }),
    updateUser:builder.mutation({
      query: ({userId, ...body}) => ({
        url:`/users/${userId}`,
        method:'PUT',
        body
      }),
      invalidatesTags:['USERS']
    }),
    deleteUser: builder.mutation({
      query: (userId: number) => ({
        url: `/users/${userId}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['USERS']
    })
  })
});

export const { useGetUsersQuery, useGetUserQuery, useUpdateUserMutation, useDeleteUserMutation } = userApi;

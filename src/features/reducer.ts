import appReducer from './slices/appSlice'
import authReducer from './slices/authSlice'

const rootReducer = {
    app:appReducer,
    auth:authReducer
};

export default rootReducer;
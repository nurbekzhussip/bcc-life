import { createSelector, createSlice, Reducer } from '@reduxjs/toolkit'
import { RootState } from '../store'

const initialState = {
  isInit: false,
  coins: 1200,
  items: [] as any
}

type InitialStateType = typeof initialState

const selectApp = (state:RootState) => state.app
export const selectIsInit = createSelector(selectApp, state => state.isInit)
export const selectCoins = createSelector(selectApp, state => state.coins)
export const selectBuyedItems = createSelector(selectApp, state => state.items)


export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    resetState:() => initialState,
    iterateCoin:(state) => {
      state.coins += 1
    },
    setIsInit: (state) => {
      state.isInit = true
    },
    buyProduct:(state, {payload}) => {
      state.items = [...state.items, payload ]
      state.coins -= payload.cost
    }
  },
})

export const { setIsInit, resetState, buyProduct, iterateCoin } = appSlice.actions

export default appSlice.reducer as Reducer<InitialStateType>
import { createSelector, createSlice, Reducer } from '@reduxjs/toolkit';
import { RootState } from '../store';

const initialState = {
  isAuth:false,
  me: null as any,
};

type InitialStateType = typeof initialState;

const selectAuth = (state:RootState) => state.auth
export const selectIsAuth = createSelector(selectAuth, (state) => state.isAuth)
export const selectAuthUser = createSelector(selectAuth, (state) => state.me)


export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    resetUser: (state) => {
      state.isAuth = false
      state.me = null

      localStorage.removeItem("accessToken")
      localStorage.removeItem("refreshToken")
    },
    setUser: (state, { payload }) => {
      state.isAuth = true
      state.me = payload;

      localStorage.setItem("accessToken", payload.accessToken)
      localStorage.setItem("refreshToken", payload.refreshToken)
    }
  }
});

export const { setUser, resetUser } = authSlice.actions;

export default authSlice.reducer as Reducer<InitialStateType>;

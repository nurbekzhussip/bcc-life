import { configureStore } from '@reduxjs/toolkit'

import { baseEmptyAPI } from './api/baseQuery'
import rootReducer from './reducer'

export const store = configureStore({
  reducer: {
    ...rootReducer,
    [baseEmptyAPI.reducerPath]: baseEmptyAPI.reducer
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(baseEmptyAPI.middleware),
  devTools: process.env.NODE_ENV !== 'production'
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
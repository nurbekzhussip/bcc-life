import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { Button, Card, Col, Row } from "antd";
import s from './style.module.css'

const { Meta } = Card;

const Benefits:FC = () => {
    const items = [
        {
            title: 'Приобретайте технику Samsung со скидкой до 50%',
            description: 'Как активировать эту скидку? Зайти на сайт Samsung.kz Зарегистрироваться на сайте с помощью рабочей почты с доменом @bcc.kz',
            link: ''
        },
        {
            title: 'Прокачивайте свои знания на Skillbox и платите меньшую сумму за курсы',
            description: 'Как активировать эту скидку? Зайти на сайт skillbox.kz Зарегистрироваться на сайте с помощью рабочей почты с доменом @bcc.kz',
            link: ''
        },
        {
          title: 'Накорми себя и своих коллег в офисах Bank CenterCredit и не плати за доставку в Chocofood',
          description: 'Как активировать эту скидку? Зайти на сайт либо скачать приложение Chocofood Зарегистрироваться  с помощью рабочей почты с доменом @bcc.kzБыть в офисах Bank CenterCredit',
          link: ''
      },
    ]
    return <>
    <div className="site-card-wrapper">
        <Row gutter={16}>
          {
            items.map((item:any, index) => {
                return <Col key={index} span={6} className={s.item}>
                <Card
                >
                  <div className={s.meta}>
                  <Meta title={item.title} description={item.description} />
                  {/* <h2 className={s.price}>{item.title}</h2>
                  <p>{item.description}</p> */}
                  <Button type="primary" className={s.button}>Посмотреть</Button>
                  </div>
                </Card>
              </Col>
            })
          }

        </Row>
      </div>
    </>
}

export default Benefits
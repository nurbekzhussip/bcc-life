import { Button } from 'antd'
import React, { FC } from 'react'
import s from './style.module.css'

const Debug:FC = () => {
    const releases = [
        {
            title: 'Приложение BCC.KZ (Android)',
            link: '#',
            description: 'В новой версии мы сделали пару исправлений, и возможно вы их даже не заметили. Спасибо, что вы с нами! С уважением, команда BCC.KZ',
            version: '1.0.3'
        },
        {
            title: 'Приложение BCC.KZ (IOS)',
            link: '#',
            description: 'В новой версии внесены правки в оплате услуг Алсеко, считывании QR кода для Skipass, а также внесены изменения при округлении сумм при конвертации. Спасибо, что вы с нами.',
            version: '1.0.3'
        },
    ]
    return <div className={s.wrap}>

        {
            releases.map((release) => {
                return <div className={s.item}>
                    <h2>{release.title}</h2>
                    <p>{release.description}</p>
                    <div style={{display:'flex', gap:'16px', alignItems:'center'}}>
                    <a href="#">Download v{release.version}</a>
                    <Button>Оставить отзыв</Button>
                    </div>
                </div>
            })
        }
    </div>
}

export default Debug
export { default as LogoutPage } from './logout/ui';
export { default as HomePage } from './home/ui';
export { default as UsersPage } from './users/ui';
export { default as TemplatesPage } from './templates/ui';
export { default as ShopPage } from './shop/ui';
export { default as VoicePage } from './voice/ui';
export { default as EventsPage } from './events/ui';
export { default as NewsPage } from './news/ui';
export { default as BenefitsPage } from './benefits/ui';
export { default as DebugPage } from './debug/ui';
export { default as ProfilePage } from './profile/ui';










import React, { FC, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { useLogoutQuery } from '../../features/api/authApi';
import { resetUser } from '../../features/slices/authSlice';

const Logout: FC = () => {
  const dispatch = useDispatch();
  const { isFetching, isSuccess } = useLogoutQuery('');

  useEffect(() => {
    if (isSuccess) {
      dispatch(resetUser());
    }
  }, [dispatch, isSuccess]);

  if (isFetching) {
    return <>...loading</>;
  }

  if (isSuccess) {
    return <Navigate to="/login" />;
  }

  return null;
};

export default Logout;

import { Button, Card, Col, Row  } from 'antd';
import clsx from 'clsx';
import React, { FC, useState } from 'react'
import { Link } from 'react-router-dom'
import image1 from '../../assets/image1.png'
import image2 from '../../assets/image2.png'

import s from './style.module.css';
const { Meta } = Card;

const News:FC = () => {
    const [state, setState] = useState(0)
    const categories = [
        {
            id:0,
            title: 'Все',
        },
        {
            id:1,
            title: 'BCC People',
        },
        {
            id:2,
            title: 'BCC Жылу',
        },
        {
            id:3,
            title: 'BCC Events',
        }
    ]

    const news = [
        {
            id:1,
            title: 'Два сотрудника Bank CenterCredit покорили Эверест',
            avatar: image1,
            tag: 'BCC People',
            description: 'Накануне, 27 января двое сотрудников Bank CenterCredit забрались на высочайшую гору на планете - Эверест'
        },
        {
            id: 2,
            title: 'Bank CenterCredit отправил сотрудника на Луну',
            avatar: image2,
            tag: 'BCC People',
            description: 'Сотрудник Bank CenterCredit установил флаг банка на спутнике Земли'
        }
    ]


const handleClick = (id:number) => () => {
    setState(id)
}

    return <>
    <div className={s.items}>
        {
            categories.map((item) => {
                return <span key={item.id} className={clsx(s.item, state === item.id && s.active)} onClick={handleClick(item.id)}># {item.title}</span>
            })
        }
    </div>
    <div className={s.news}>
    <Row gutter={16}>
          {
            news.map((item:any, index) => {
                return <Col key={index} xs={24} sm={12} md={8} lg={8} xl={8} className={s.newsItem}>
                <Card
                className={s.card}
                    cover={<img alt="example" src={item.avatar} className={s.avatar}/>}
                >
                     <Meta title={item.title} description={item.description}/>
                  <div className={s.meta}>
                  {/* <h2 className={s.price}>{item.title}</h2>
                  <p>{item.description}</p> */}
                  <div className={s.bottom}>
                    <span>#{item.tag}</span>
                  <a href="#">Подробнее</a>
                  </div>
                  </div>
                </Card>
              </Col>
            })
          }

        </Row>
    </div>
    </>
}

export default News
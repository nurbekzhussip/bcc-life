import { Avatar, Button, QRCode, Tabs } from "antd";
import React, { FC } from "react";
import { Link } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";
import s from "./style.module.css";
import { useSelector } from 'react-redux';
import { selectCoins } from '../../features/slices/appSlice';
import { RootState } from '../../features/store';
import imgCoin from '../../assets/coin.png'

const items: any = [
    {
        key: '0',
        label: `Мой QR`,
        children: <>
        <QRCode
            errorLevel="H"
            value="https://bcc.kz/"
            icon="https://play-lh.googleusercontent.com/CaDX3Gs8XPdPwxSMBDjgDUZ4myDM48lTDdZCyqRu8C-Y0LRFv5Mbczj0pXP4AV8xrLzO"
        />
        </>,
      },
    {
      key: '1',
      label: `Моя команда`,
      children: `Content of Tab Pane 1`,
    },
    {
      key: '2',
      label: `Мои отпуска`,
      children: `Content of Tab Pane 2`,
    },
    {
      key: '3',
      label: `Мои доступы`,
      children: `Content of Tab Pane 3`,
    },{
        key: '4',
        label: `Мое рабочее место`,
        children: `Content of Tab Pane 4`,
      },
      {
        key: '5',
        label: `Мои документы`,
        children: `Content of Tab Pane 5`,
      },{
        key: '6',
        label: `Мое обучение`,
        children: `Content of Tab Pane 6`,
      },
  ];

const Profile: FC = () => {
    const coins = useSelector((state:RootState) => selectCoins(state))

  return (
    <div className={s.wrap}>
      <div className={s.info}>
        <Avatar icon={<UserOutlined />} size="large" />
        <div>
          <h3>Артем Мигалкин</h3>
          <br />
          <p>Блок по розничному бизнесу</p>
          <p>CX-исследователь</p>
          <br />
          <div>
            <label htmlFor="">BCC-Coin:</label>
            <span className={s.coins}>{coins}<img src={imgCoin} style={{
                width:'16px'
            }}/></span>
          </div>
        </div>
      </div>
      <Tabs defaultActiveKey="0" items={items}  className={s.tab}/>
    </div>
  );
};

export default Profile;

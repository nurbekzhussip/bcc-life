import React, { FC } from "react";
import { Button, Card, Col, Row } from "antd";
import { numberWithSpaces } from '../../shared/helpers/numberFormat';
import { useDispatch, useSelector } from 'react-redux';
import { buyProduct, selectBuyedItems } from '../../features/slices/appSlice';
import { RootState } from '../../features/store';
import s from './style.module.css'

const Shop: FC = () => {
const dispatch = useDispatch()
const buyedItems:any[] = useSelector((state:RootState) => selectBuyedItems(state))

    const products = [
        {
          id: 1,
            title: 'термос',
            src: 'https://bccstore.kz/images/thumbnails/270/270/detailed/8/термос.png',
            price: 16
        },
        {
            id: 2,
            title: 'плед',
            src: 'https://bccstore.kz/images/thumbnails/270/270/detailed/8/плед.png',
            price: 20
        }, 
        {
            id: 3,
            title:'зонт',
            src: 'https://bccstore.kz/images/thumbnails/270/270/detailed/8/зонт.png',
            price: 30
        },
        {
          id: 4,
            title: 'часы',
            src:'https://bccstore.kz/images/thumbnails/270/270/detailed/8/часы.png',
            price: 50
        },
        {
          id:5,
            title: 'часы',
            src:'https://bccstore.kz/images/thumbnails/270/270/detailed/8/стакан.png',
            price: 30
        }
    ]
    
    const handleShop = (itemId:number, cost:number) => () => {
      dispatch(buyProduct({
        id: itemId,
        cost: cost
      }))
    }

  return (
    <>
      <div className="site-card-wrapper">
        <Row gutter={16}>
          {
            products.map((product:any) => {
                return <Col key={product.id} span={6} className={s.item}>
                <Card
                  cover={
                    <img
                      alt={product.id}
                      src={product.src}
                    />
                  }
                >
                  <div className={s.meta}>
                  <h4 className={s.price}>{numberWithSpaces(product.price)}</h4>
                  {
                    !buyedItems.find((buyedItem:any) => {
                      return buyedItem.id === product.id
                    }) ? <Button type="primary" onClick={handleShop(product.id, product.price)}>Обменять</Button> : <Button type="text" disabled>Приобретено</Button>
                  }
                  </div>
                </Card>
              </Col>
            })
          }
        </Row>
      </div>
    </>
  );
};

export default Shop;

import React, { FC, useEffect, useState } from 'react';
import {
  Button,
  Drawer,
  PaginationProps,
  Popconfirm,
  Space,
  Table
} from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import {
  useDeleteTemplateMutation,
  useGetTemplateByIdQuery,
  useGetTemplatesQuery,
} from '../../features/api/templateApi';
import { ITemplate } from '../../shared/interfaces/Template';
import { TemplateForm } from '../../entities';
import s from './style.module.css';


const Templates: FC = () => {
  const [open, setOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedItem, setSelectedItem] = useState<number | null>(null);

  const { data, isFetching } = useGetTemplatesQuery('');
  const {
    data: selectedData,
    isFetching: isSelectedLoading,
    isSuccess: isSelectedSuccess
  } = useGetTemplateByIdQuery(selectedItem, {
    skip: !selectedItem
  });

  const [deleteTemplate, { isLoading: isDeleteLoading }] =
    useDeleteTemplateMutation();

  useEffect(() => {
    if (isSelectedSuccess) {
      setOpen(true);
    }
  }, [isSelectedSuccess, selectedData]);

  const onOpen = () => {
    setSelectedItem(null)
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const onChange: PaginationProps['onChange'] = (page, pageSize) => {
    setCurrentPage(page);
  };

  const onRemove = (id: number) => () => {
    deleteTemplate(id);
  };

  const onEdit = (id: number) => () => {
    setSelectedItem(id);
    setOpen(true);
  };

  const columns: ColumnsType<ITemplate> = [
    {
      title: 'Заголовок',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: 'Текст',
      dataIndex: 'text',
      key: 'text'
    },
    {
      title: 'Действие',
      dataIndex: 'id',
      key: 'action',
      render: (id: number, record: ITemplate) => {
        return (
          <Space size="middle">
            <Button onClick={onEdit(id)} type="text">
              <EditOutlined />
            </Button>
            <Popconfirm
              title={`Вы точно хотите удалить ${record.title}?`}
              onConfirm={onRemove(id)}
              okText="Да"
              cancelText="Отмена"
            >
              <Button type="text" danger>
                <DeleteOutlined />
              </Button>
            </Popconfirm>
          </Space>
        );
      }
    }
  ];

  return (
    <>
      <div className={s.topMenu}>
        <Button onClick={onOpen} icon={<PlusOutlined />}>
          Добавить
        </Button>
      </div>
      <Table
        key={data?.items}
        dataSource={data?.items || []}
        columns={columns}
        loading={isFetching || isDeleteLoading || isSelectedLoading}
        pagination={{
          total: data?.meta.totalPages,
          current: currentPage,
          pageSize: data?.meta.itemsPerPage,
          onChange: onChange
        }}
      />
      <Drawer
        title="Добавить шаблон"
        placement="right"
        onClose={onClose}
        open={open}
      >
        <TemplateForm key={open.toString()} item={selectedData} onClose={onClose} />
      </Drawer>
    </>
  );
};

export default Templates;

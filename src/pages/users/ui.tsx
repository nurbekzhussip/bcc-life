import React, { FC, useState } from 'react';
import { useSelector } from 'react-redux';
import dayjs from 'dayjs';
import { Button, Drawer, PaginationProps, Popconfirm, Space, Table, Tag } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import {
  useDeleteUserMutation,
  useGetUsersQuery
} from '../../features/api/userApi';
import { selectAuthUser } from '../../features/slices/authSlice';
import { RootState } from '../../features/store';
import { UserEdit } from '../../entities/user';
import { UserRolesEnum } from '../../shared/enums/User';

const Users: FC = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [selectedUserId, setSelectedUserId] = useState<number | null>(null)
  const authUser = useSelector((state: RootState) => selectAuthUser(state));
  
  const [deleteUser, { isLoading: isDeleteLoading }] = useDeleteUserMutation();
  const { data, isFetching } = useGetUsersQuery({
    page: currentPage,
    limit: 20
  });

  const handleEdit = (userId: number) => () => {
    setSelectedUserId(userId)
    setOpen(true)
  };

  const handleRemove = (userId: number) => () => {
    deleteUser(userId);
  };

  const onClose = () => {
    setSelectedUserId(null)
    setOpen(false);
  };

  const columns = [
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    },
    {
      title: 'Роль',
      dataIndex: 'role',
      key: 'role',
      render: (value: string) => (
        <Tag color="green" key={value}>
          {value.toUpperCase()}
        </Tag>
      )
    },
    {
      title: 'Дата',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (value: string) => {
        return `${dayjs(value).format('MM-DD-YYYY HH:mm')}`;
      }
    },
    {
      title: 'Действие',
      key: 'action',
      render: (_: string, record: any) => {
        if (record.id === authUser.id) {
          return null;
        }

        if(authUser?.role !== UserRolesEnum.ADMIN){
          return null
        }

        return (
          <Space size="middle">
            <Button onClick={handleEdit(record.id)} type="text">
              <EditOutlined />
            </Button>
            <Popconfirm
              title={`Вы точно хотите удалить ${record.email}?`}
              onConfirm={handleRemove(record.id)}
              okText="Да"
              cancelText="Отмена"
            >
              <Button type="text" danger>
                <DeleteOutlined />
              </Button>
            </Popconfirm>
          </Space>
        );
      }
    }
  ];

  const onChange: PaginationProps['onChange'] = (page, pageSize) => {
    setCurrentPage(page);
  };

  return (
    <>
    <Table
      key={data?.meta}
      dataSource={data?.items || []}
      columns={columns}
      loading={isFetching || isDeleteLoading}
      pagination={{
        total: data?.meta.totalPages,
        current: currentPage,
        pageSize: data?.meta.itemsPerPage,
        onChange: onChange
      }}
    />
    <Drawer
        title="Редактировать"
        placement="right"
        onClose={onClose}
        open={open}
      >
       <UserEdit userId={selectedUserId as number} onClose={onClose}/>
      </Drawer>
    </>
  );
};

export default Users;

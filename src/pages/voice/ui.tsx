import React, { FC, useState } from 'react'
import { Link } from 'react-router-dom'
import { Button,Badge, Card, Col, Row, Alert } from "antd";
import { LikeOutlined,MessageOutlined,PlusOutlined } from '@ant-design/icons';

import s from './style.module.css'

const { Meta } = Card;

const Voice:FC = () => {
    const [state, setState] = useState(5)
    const ideas = [{
        title: 'Предлагаю добавить relax-комнаты в офисе в БЦ "Ramstore"',
        description: "В данный момент в офисе в БЦ 'Ramstore' нет relax-комнат, где сотрудник может полежать на кроватке, либо поиграть в плейстейшн в обеденный перерыв. Часто вижу, как коллеги просто лежать на своих рабочих столах, а из досуга есть только теннисный столик. Предлагаю выделить какой-нибудь зал или помещение под релакс-комнату.",
        like: 6
    },
    {
      title: 'Не хватает вендинговых машин на Рамсторе',
      description: "Я устал бегать в магазин. Хочу я шоколадку (я люблю шоколадки), но почему в Рамсторе не хватает вендинговых машин!!! Давайте их добавим!",
      like: 2
  }
  ]

    const handleClick = () => {
        setState((prev) => prev+1)
    }
    
    return <>
     <div className="site-card-wrapper">
      <div className='top'>
        <Button type='primary' icon={<PlusOutlined />}>Добавить</Button>
      </div>
      <Alert
      message="Добавьте идею и получите 10 coins"
      type="info"
      showIcon
    />
      <br />
        <Row gutter={16}>
          {
            ideas.map((idea:any, index) => {
                return <Col key={index} span={6} className={s.item}>
                <Card 
                >
                   <Meta title={idea.title} description={idea.description} />
                  <div className={s.meta}>
                  {/* <p className={s.description}>{idea.description}</p> */}
                  <div className={s.likes}>
                  <span>
                  {state}<LikeOutlined onClick={handleClick}/>
                  </span>
                  <Button className={s.button} icon={<MessageOutlined />} type="text"/></div>
                  </div>
                </Card>
              </Col>
            })
          }

        </Row>
      </div>
    </>
}

export default Voice
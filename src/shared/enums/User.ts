export enum UserRolesEnum {
    USER = 'USER',
    VIEWER = 'VIEWER',
    MODERATOR = 'MODERATOR',
    ADMIN = 'ADMIN',
}

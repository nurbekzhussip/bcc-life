

export interface IMember {
    id:number;
    firstname:string;
    lastname:string;
    messenger: any;
    username:string;
    fromId:number;
    isBotEnabled:boolean;
    createdAt: string;
    lastMessage?:any
}
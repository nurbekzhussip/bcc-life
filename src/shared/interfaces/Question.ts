type QuestionInputType = 'KEYBOARD' | 'TEXT';

export interface IQuestion {
  id: number;
  title: string;
  type: QuestionInputType;
  nextQuestionId: null | number;
  createdAt: string;
}

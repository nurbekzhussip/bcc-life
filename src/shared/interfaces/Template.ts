export interface ITemplate {
    id:number;
    title:string;
    text:string;
}